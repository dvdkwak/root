<?php

class beheerMenu extends db
{
    public function quickOverview($where)
    {
        $mysqli = $this->connect();
        $query = 'SELECT '.$where.' 
                    FROM tbl_menu 
                    WHERE 1';
        $result = $mysqli->query($query);
        while($data = $result->fetch_assoc())
        {
            $return[] = $data;
        }
        return $return;
    }

    public function removeMenu($id, $name)
    {
        $mysqli = $this->connect();
        $id = $mysqli->real_escape_string($id);
        $page_title = $mysqli->real_escape_string($name);
        $query = 'DELETE FROM tbl_menu
                        WHERE id='.$id;
        $error = $mysqli->query($query);
        if(empty($error)){
            $return = array("code"=>"1", "message"=>"<strong>Something went wrong...</strong>");
            return $return;
        }elseif($error == "1"){
            $return = array("code"=>"2", "message"=>"<strong>The page has been ".$name." removed!</strong>");
            return $return;
        }else{
            $return = array("code"=>"1", "message"=>"<strong>Something went really wrong...</strong>");
            return $return;
        }
    }

    public function detailOverview($id)
    {
        $mysqli = $this->connect();
        $query = 'SELECT * 
                    FROM tbl_menu 
                    WHERE id="'. $id .'"';
        $result = $mysqli->query($query);
        $data = $result->fetch_assoc();
        $return = $data;
        return $return;
    }

    public function saveChanges($id, $name, $FK_pages_id, $parent_id){
        $mysqli = $this->connect();
        $id = $mysqli->real_escape_string($id);
        $name = $mysqli->real_escape_string($name);
        $FK_pages_id = $mysqli->real_escape_string($FK_pages_id);
        $parent_id = $mysqli->real_escape_string($parent_id);
        $query = 'UPDATE tbl_menu
                    SET `name` = "'.$name.'", 
                        `FK_pages_id` = "'.$FK_pages_id.'",
                               `parent_id` = "'.$parent_id.'"
                    WHERE `id` = "'.$id.'"';
        $error = $mysqli->query($query);
        if(empty($error)){
            $return = array("code"=>"2", "message"=>"FAILED YO BAKKA!");
            return $return;
        }elseif($error == "1"){
            $return = array("code"=>"1", "message"=>"Changes succesfully saved!");
            return $return;
        }else{
            return "NO CLUE";
        }
    }

    public function addPage($name, $FK_pages_id, $parent_id){
        $mysqli = $this->connect();
        $id = "";
        $name = $mysqli->real_escape_string($name);
        $FK_pages_id = $mysqli->real_escape_string($FK_pages_id);
        $parent_id = $mysqli->real_escape_string($parent_id);
        $query = 'INSERT INTO tbl_pages(
                                `id`, `name`, `FK_pages_id`,
                               `parent_id`)
                      VALUES(
                              "'.$id.'", "'.$name.'", "'.$FK_pages_id.'",
                              "'.$fgh.'", "'.$meta_keywords.'", "'.$FK_module_id.'",
                              "'.$lock.'", "'.$lock_location.'", "'.$meta_description.'",
                              "'.$content.'")';
        $error = $mysqli->query($query);
        if(empty($error)){
            $return = array("code"=>"2", "message"=>"FAILED YO BAKKA!");
            return $return;
        }elseif($error == "1"){
            $return = array("code"=>"1", "message"=>"Succesfully created a page!");
            return $return;
        }else{
            return "NO CLUE";
        }
    }
}