<?php

class beheerPage extends db{

    public function getContent( $url ){
        $params = $url;
        $params = substr($params, 1);
        $params = explode('/', $params);

        switch ( $params['0'] ){
            case "home":
                $return = array("styleSheet"=>"", "location"=>"beheer_pages/home.php", "title"=>"beheer | home");
                return $return;
                break;
            case "page":
                $return = array('styleSheet'=>'<link rel="stylesheet" href="/beheer/css/beheerPages.css">', "location"=>"beheer_pages/pages.php", "title"=>"beheer | pages");
                return $return;
            case "article":
                $return = array('styleSheet'=>'<link rel="stylesheet" href="/beheer/css/beheerPages.css">', "location"=>"beheer_pages/articles.php", "title"=>"beheer | articles");
                return $return;
            case "menu":
                $return = array('styleSheet'=>'<link rel="stylesheet" href="/beheer/css/beheerPages.css">', "location"=>"beheer_pages/menu.php", "title"=>"beheer | menu");
                return $return;
                break;
            default:
                $return = array("styleSheet"=>"", "location"=>"beheer_pages/404.php", "title"=>"Woops...");
                return $return;
        }
    }

}

?>
