<?php

class beheerPages extends db
{
    public function quickOverview()
    {
        $mysqli = $this->connect();
        $query = 'SELECT `id`, `page_title`, `meta_title`, `url`, `lock` 
                    FROM tbl_pages 
                    WHERE 1';
        $result = $mysqli->query($query);
        while($data = $result->fetch_assoc())
        {
            $return[] = $data;
        }
        return $return;
    }

    public function detailOverview($id)
    {
        $mysqli = $this->connect();
        $query = 'SELECT * 
                    FROM tbl_pages 
                    WHERE id="'. $id .'"';
        $result = $mysqli->query($query);
        $data = $result->fetch_assoc();
        if($data['FK_module_id'] > 0){
            $query = 'SELECT * 
                        FROM tbl_pages
                        INNER JOIN tbl_modules 
                        ON tbl_pages.FK_module_id = tbl_modules.id
                        WHERE tbl_pages.id="'. $id .'"';
            $result = $mysqli->query($query);
            $data = $result->fetch_assoc();
        }
        $return = $data;
        return $return;
    }

    public function saveChanges($id, $page_title, $meta_title, $url, $meta_keywords, $FK_module_id, $lock,
                                $lock_location, $meta_description, $content){
        $mysqli = $this->connect();
        $id = $mysqli->real_escape_string($id);
        $page_title = $mysqli->real_escape_string($page_title);
        $meta_title = $mysqli->real_escape_string($meta_title);
        $url = $mysqli->real_escape_string($url);
        $meta_keywords = $mysqli->real_escape_string($meta_keywords);
        $FK_module_id = $mysqli->real_escape_string($FK_module_id);
        $lock = $mysqli->real_escape_string($lock);
        $lock_location = $mysqli->real_escape_string($lock_location);
        $meta_description = $mysqli->real_escape_string($meta_description);
        $content = $mysqli->real_escape_string($content);
        $query = 'UPDATE tbl_pages
                    SET `page_title` = "'.$page_title.'", 
                        `meta_title` = "'.$meta_title.'",
                               `url` = "'.$url.'",
                     `meta_keywords` = "'.$meta_keywords.'",
                      `FK_module_id` = "'.$FK_module_id.'",
                              `lock` = "'.$lock.'",
                     `lock_location` = "'.$lock_location.'",
                  `meta_description` = "'.$meta_description.'",
                           `content` = "'.$content.'"
                    WHERE `id` = "'.$id.'"';
        $error = $mysqli->query($query);
        if(empty($error)){
            $return = array("code"=>"2", "message"=>"FAILED YO BAKKA!");
            return $return;
        }elseif($error == "1"){
            $return = array("code"=>"1", "message"=>"Changes succesfully saved!");
            return $return;
        }else{
            return "NO CLUE";
        }
    }

    public function addPage($page_title, $meta_title, $url, $meta_keywords, $FK_module_id, $lock,
                            $lock_location, $meta_description, $content){
        $mysqli = $this->connect();
        $id = "";
        $page_title = $mysqli->real_escape_string($page_title);
        $meta_title = $mysqli->real_escape_string($meta_title);
        $url = $mysqli->real_escape_string($url);
        $meta_keywords = $mysqli->real_escape_string($meta_keywords);
        $FK_module_id = $mysqli->real_escape_string($FK_module_id);
        $lock = $mysqli->real_escape_string($lock);
        $lock_location = $mysqli->real_escape_string($lock_location);
        $meta_description = $mysqli->real_escape_string($meta_description);
        $content = $mysqli->real_escape_string($content);
        $query = 'INSERT INTO tbl_pages(
                                `id`, `page_title`, `meta_title`,
                               `url`, `meta_keywords`, `FK_module_id`,
                              `lock`, `lock_location`, `meta_description`, `content`)
                      VALUES(
                              "'.$id.'", "'.$page_title.'", "'.$meta_title.'",
                              "'.$url.'", "'.$meta_keywords.'", "'.$FK_module_id.'",
                              "'.$lock.'", "'.$lock_location.'", "'.$meta_description.'",
                              "'.$content.'")';
        $error = $mysqli->query($query);
        if(empty($error)){
            $return = array("code"=>"2", "message"=>"FAILED YO BAKKA!");
            return $return;
        }elseif($error == "1"){
            $return = array("code"=>"1", "message"=>"Succesfully created a page!");
            return $return;
        }else{
            return "NO CLUE";
        }
    }

    public function removePage($id, $page_title){
        $mysqli = $this->connect();
        $id = $mysqli->real_escape_string($id);
        $page_title = $mysqli->real_escape_string($page_title);
        $query = 'DELETE FROM tbl_pages
                        WHERE id='.$id;
        $error = $mysqli->query($query);
        if(empty($error)){
            $return = array("code"=>"1", "message"=>"<strong>Something went wrong...</strong>");
            return $return;
        }elseif($error == "1"){
            $return = array("code"=>"2", "message"=>"<strong>The page has been ".$page_title." removed!</strong>");
            return $return;
        }else{
            $return = array("code"=>"1", "message"=>"<strong>Something went really wrong...</strong>");
            return $return;
        }
    }
}