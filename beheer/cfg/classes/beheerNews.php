<?php

class beheerNews extends db
{
    public function quickOverView(){
        $mysqli = $this->connect();
        $query = 'SELECT `id`, `title`, `summary`, `thumb_nail`, `author`
                    FROM tbl_news
                    WHERE 1';
        $result = $mysqli->query($query);
        while($data = $result->fetch_assoc()){
            $return[] = $data;
        }
        return $return;
    }

    public function detailOverview($id){
        $mysqli = $this->connect();
        $mysqli->real_escape_string($id);
        $query = 'SELECT *
                    FROM `tbl_news`
                    WHERE `id` = '.$id;
        $result = $mysqli->query($query);
        if(!empty($result)){
            $return = $result->fetch_assoc();
        }else{
            $return = false;
        }
        return $return;
    }

    public function saveChanges($id, $title, $summary, $content, $thumb_nail, $author){
        $mysqli = $this->connect();
        $mysqli->real_escape_string($id);
        $mysqli->real_escape_string($title);
        $mysqli->real_escape_string($summary);
        $mysqli->real_escape_string($content);
        $mysqli->real_escape_string($thumb_nail);
        $mysqli->real_escape_string($author);
        $query = 'UPDATE `tbl_news`
                     SET `title` = "'.$title.'",
                         `summary` = "'.$summary.'",
                         `content` = "'.$content.'",
                         `thumb_nail` = "'.$thumb_nail.'",
                         `author` = "'.$author.'"
                   WHERE `id` = '.$id;
        $error = $mysqli->query($query);
        if(empty($error)){
            $return = array("code"=>"2", "message"=>"FAILED YO BAKKA!");
            return $return;
        }elseif($error == "1"){
            $return = array("code"=>"1", "message"=>"Changes successfully saved!");
            return $return;
        }else{
            return "NO CLUE";
        }
    }

    public function addArticle($title, $summary, $content, $thumb_nail, $author){
        $mysqli = $this->connect();
        $title = $mysqli->real_escape_string($title);
        $summary = $mysqli->real_escape_string($summary);
        $content = $mysqli->real_escape_string($content);
        $thumb_nail = $mysqli->real_escape_string($thumb_nail);
        $author = $mysqli->real_escape_string($author);
        $query = 'INSERT INTO tbl_news(
                              `id`, `title`, `summary`,
                              `content`, `thumb_nail`,
                              `author`)
                       VALUES (
                              "", "'.$title.'", "'.$summary.'",
                              "'.$content.'", "'.$thumb_nail.'", "'.$author.'"
                       )';
        $error = $mysqli->query($query);
        if(empty($error)){
            $return = array("code"=>"2", "message"=>"FAILED YO BAKKA!");
            return $return;
        }elseif($error == "1"){
            $return = array("code"=>"1", "message"=>"Succesfully created an Article!");
            return $return;
        }else{
            return "NO CLUE";
        }
    }

    public function removeArticle($id, $title){
        $mysqli = $this->connect();
        $id = $mysqli->real_escape_string($id);
        $title = $mysqli->query($title);
        $query = 'DELETE FROM tbl_news
                        WHERE `id`='.$id;
        $error = $mysqli->query($query);
        if(empty($error)){
            $return = array("code"=>"1", "message"=>"<strong>Something went wrong...</strong>");
            return $return;
        }elseif($error == "1"){
            $return = array("code"=>"2", "message"=>"<strong>The article has been ".$title." removed!</strong>");
            return $return;
        }else{
            $return = array("code"=>"1", "message"=>"<strong>Something went really wrong...</strong>");
            return $return;
        }
    }
}