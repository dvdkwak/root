<?php
  include_once( "../cfg/config.php" );
  include_once( "cfg/beheerConfig.php" );
  $user = new user();
  $user->lock( "../login", "beheer" );

  if( isset( $_GET['url'] ) ){
      $logoutCheck = explode("/", $url);
      if(in_array("logout", $logoutCheck)){
          $user->logOut( "/home" );
      }
  }

    $content = new beheerPage();
    $content = $content->getContent( $url );
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/fontawesome/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/index.css" rel="stylesheet">
    <link href="/beheer/css/beheer.css" rel="stylesheet">
    <?php echo $content['styleSheet']; ?>
    <!-- title van de site -->
    <title><?php echo $content['title']; ?></title>
  </head>
  <body>
	<div class="container-fluid">

    <!-- HEADER -->
    <div class="row beheer-header-rood">
      <div class="col-xs-12">
        <div class="beheer-header">
          <a href="/beheer/"><h1>Welcome at YouCode</h1></a>
        </div>
        <div class="beheer-header-uitlog">
          <a href="logout">
            <button onclick="">
              <i class="fa fa-2x fa-sign-out" aria-hidden="true"></i>
            </button>
          </a>
        </div>
      </div>
    </div>

    <div class="row">
  <!-- Content code -->
      <div class="col-lg-9">
        <div class="row">
          <div class="beheer-content">
    <?php
      include_once($content['location']);
    ?>
          </div>
        </div>
        <div class="row">
          <div class="beheer-supp-header"></div>
        </div>
    <!-- SLUITEN VAN DE CONTAINER -->
      </div>

      <div class="col-lg-3">
    <?php
      include_once( "beheer_pages/beheer_menu.php" );

      if( $url != "/home" || $url != "/" ){
    ?>

    <!-- HIER KOMT EEN HIDDEN ROW VOOR DE SIDEBAR VAN HET BEHEER -->
    <div class="row">
      <div class="hidden-xs hidden-sm hidden-md col-lg-6">
        <a href="/beheer/article">
          <div class="beheer-option-C beheer-option">
            <h1>Articles</h1>
            <i class="fa fa-5x fa-newspaper-o" aria-hidden="true"></i>
          </div>
        </a>
      </div>
      <div class="hidden-xs hidden-sm hidden-md col-lg-6">
        <a href="/beheer/user">
          <div class="beheer-option-E beheer-option">
            <h1>Users</h1>
            <i class="fa fa-5x fa-users" aria-hidden="true"></i>
          </div>
        </a>
      </div>
    </div>

    <div class="row">
      <div class="hidden-xs hidden-sm hidden-md col-lg-6">
        <a href="/beheer/module">
          <div class="beheer-option-A beheer-option">
            <h1>Modules</h1>
            <i class="fa fa-5x fa-cogs" aria-hidden="true"></i>
          </div>
        </a>
      </div>
      <div class="hidden-xs hidden-sm hidden-md col-lg-6">
        <a href="/beheer/inbox">
          <div class="beheer-option-D beheer-option">
            <h1>Inbox</h1>
            <i class="fa fa-5x fa-inbox" aria-hidden="true"></i>
          </div>
        </a>
      </div>
    </div>

<?php
  }
?>

    <!-- HIER KOMT ROW 2 (FOOTER BEHEER) -->
        <div class="row">
          <div class="beheer-footer"></div>
        </div>
      </div>

    <!-- eindigen grootste container-->
    </div>
  </div>
  </body>
  <script src="/jquery/jquery.min.js"></script>
  <script src="/bootstrap/js/bootstrap.min.js"></script>
  <script src="/js/index.js"></script>
  <script src="/beheer/js/beheer.js"></script>
  <script src="/beheer/js/beheer-pages.js"></script>
</html>
