<?php
$urlCheck = explode("/", $url);
$params = $url;
$params = substr($params, 1);
$params = explode('/', $params);

$page = new beheerPages();
$modules = new beheerModules();

if( $url == "/page" || $urlCheck['2'] == "remove") {
    //THIS SECTION SHOWS A TABLE WITH AN OVERVIEW OF ALL PAGES IN THE SYSTEM
    $table = new beheerPages();
    $tableRow = $table->quickOverview();

    if(in_array("remove", $params)){
        $error = array("code"=>"2", "message"=>"Are you sure to <strong>remove</strong> the page: <strong>".$params['3']."</strong>?");
        if(isset($_POST['action']) && $_POST['action'] == "remove"){
           $error = $page->removePage($params['2'], $params['3']);
           header("location:/beheer/page");
        }
    }
    ?>
    <div class="container-fluid">
        <h1>Overview of all pages on this site.</h1>
        <a href="page/add">
            <div class="btn alert-success pull-right">
                <i class="fa fa-plus" aria-hidden="true"></i>  Add
            </div>
        </a>
        <hr>
        <div class="alert alert-success alert-dismissable <?php if(!isset($error['code'])){echo 'hidden';}?>">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $error['message']; ?>
            <form class="pull-right" method="post">
                <input type="hidden" name="action" value="remove">
                <input type="submit" value="Yes">
            </form>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th><p>page title</p></th>
                <th><p>meta title</p></th>
                <th><p>url</p></th>
                <th><p>locked</p></th>
                <th class="text-center"><p>delete</p></th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($tableRow as $row) {
                echo '<tr class="table_hover">';
                foreach (array_slice($row, 1) as $item) {
                    echo '<td><a href="/beheer/page/' . $row['id'] . '">';
                    echo $item;
                    echo '</a></td>';
                }
                echo '<td>
                    <a href="/beheer/page/remove/'.$row['id'].'/'.$row['page_title'].'" class="table_thrash">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </a>
                      </td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
    <?php
}else{
    //__________________________________________ SECTION FOR EXISTING PAGES (UPDATES) AND CREATING NEW (INSERT)_________________________________

    if(isset($_POST['flag']) && $_POST['flag'] == "send"){
        if($params[1] != "add") {
            $error = $page->saveChanges($params['1'], $_POST['page_title'], $_POST['meta_title'], $_POST['url'],
                $_POST['meta_keywords'], $_POST['FK_module_id'], $_POST['lock'], $_POST['lock_location'],
                $_POST['meta_description'], $_POST['content']);
        }else{
            $error = $page->addPage($_POST['page_title'], $_POST['meta_title'], $_POST['url'],
                $_POST['meta_keywords'], $_POST['FK_module_id'], $_POST['lock'], $_POST['lock_location'],
                $_POST['meta_description'], $_POST['content']);
        }
    }

    $pageData = $page->detailOverview($params[1]);
    $pageTitles = $page->quickOverview();
    $moduleData = $modules->getModules();

    ?>
    <div class="container-fluid">
        <div class="alert alert-success alert-dismissable <?php if(!isset($error['code'])){echo 'hidden';}?>">
            <strong><?php echo $error['message']; ?></strong>
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
        <form action="" method="post" autocomplete="off">
            <div class="page_update">
                <div class="row">
                    <div class="col-md-4">
                        <h1>Page title</h1>
                        <input type="text" name="page_title" value="<?php echo $pageData['page_title'] ?>">
                    </div>
                    <div class="col-md-4">
                        <h1>Meta title</h1>
                        <input type="text" name="meta_title" value="<?php echo $pageData['meta_title'] ?>">
                    </div>
                    <div class="col-md-4">
                        <h1>Url</h1>
                        <input type="text" name="url" value="<?php echo $pageData['url'] ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h1>Keywords</h1>
                        <input type="text" name="meta_keywords" value="<?php echo $pageData['meta_keywords'] ?>">
                    </div>
                    <div class="col-md-6">
                        <h1>Implemented module</h1>
                        <select name="FK_module_id">
                            <?php
                            if(empty($pageData['FK_module_id'])){
                                echo '<option value="0">No module selected</option>';
                            }else {
                                echo '<option value="' . $pageData['FK_module_id'] . '">' . $pageData['name'] . '</option>';
                            }
                            foreach($moduleData AS $option){
                                echo '<option value="'.$option['id'].'">'.$option['name'].'</option>';
                            }
                            echo '<option value="0">no module selected</option>';
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h1>lock</h1>
                        <select name="lock">
                            <?php
                            if(empty($pageData['lock']) || $pageData['lock'] == "0"){
                                echo '<option value="0">unlocked</option>';
                                echo '<option value="1">locked</option>';
                            }else{
                                echo '<option value="1">locked</option>';
                                echo '<option value="0">unlocked</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <h1>Lock location</h1>
                        <select name="lock_location">
                            <?php
                            if(empty($pageData['lock_location'])){
                                echo '<option>not locked</option>';
                            }
                            foreach($pageTitles AS $item){
                                echo '<option>'.$item['url'].'</option>';
                            }
                            echo '<option></option>';
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="page_update">
                <h1>page description</h1>
                <textarea name="meta_description"><?php echo $pageData['meta_description'] ?></textarea>
            </div>
            <div class="page_update">
                <h1>content</h1>
                <textarea name="content"><?php echo $pageData['content'] ?></textarea>
            </div>
        </div>

        <div class="submit">
            <div>
                <input type="hidden" name="flag" value="send">
                <input type="submit" value="save">
            </div>
        </div>
    </form>

<?php } ?>
