<?php
$urlCheck = explode("/", $url);
$params = $url;
$params = substr($params, 1);
$params = explode('/', $params);

$menu = new beheerMenu();

if( $url == "/menu" || $urlCheck['2'] == "remove") {
    //THIS SECTION SHOWS A TABLE WITH AN OVERVIEW OF ALL PAGES IN THE SYSTEM
    $table = new beheerMenu();
    $where = 'id, name';
    $tableRow = $table->quickOverview($where);

    if(in_array("remove", $params)){
        $error = array("code"=>"2", "message"=>"Are you sure to <strong>remove</strong> menu item: <strong>".$params['3']."</strong>?");
        if(isset($_POST['action']) && $_POST['action'] == "remove"){
            $error = $menu->removeMenu($params['2'], $params['3']);
            header("location:/beheer/menu");
        }
    }
    ?>
    <div class="container-fluid">
        <h1>Overview of all menu items on this site.</h1>
        <a href="menu/add">
            <div class="btn alert-success pull-right">
                <i class="fa fa-plus" aria-hidden="true"></i>  Add
            </div>
        </a>
        <hr>
        <div class="alert alert-success alert-dismissable <?php if(!isset($error['code'])){echo 'hidden';}?>">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $error['message']; ?>
            <form class="pull-right" method="post">
                <input type="hidden" name="action" value="remove">
                <input type="submit" value="Yes">
            </form>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th><p>name</p></th>
                <th class="text-center"><p>delete</p></th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($tableRow as $row) {
                echo '<tr class="table_hover">';
                foreach (array_slice($row, 1) as $item) {
                    echo '<td><a href="/beheer/menu/' . $row['id'] . '">';
                    echo $item;
                    echo '</a></td>';
                }
                echo '<td>
                    <a href="/beheer/menu/remove/'.$row['id'].'/'.$row['name'].'" class="table_thrash">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </a>
                      </td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
    <?php
}else{
    //__________________________________________ SECTION FOR EXISTING PAGES (UPDATES) _________________________________

    if(isset($_POST['flag']) && $_POST['flag'] == "send"){
        if($params[1] != "add") {
            $error = $menu->saveChanges($params['1'], $_POST['page_title'], $_POST['meta_title'], $_POST['url'],
                $_POST['meta_keywords'], $_POST['FK_module_id'], $_POST['lock'], $_POST['lock_location'],
                $_POST['meta_description'], $_POST['content']);
        }else{
            $error = $menu->addItem($_POST['page_title'], $_POST['meta_title'], $_POST['url'],
                $_POST['meta_keywords'], $_POST['FK_module_id'], $_POST['lock'], $_POST['lock_location'],
                $_POST['meta_description'], $_POST['content']);
        }
    }

    $menuData = $menu->detailOverview($params[1]);
    $where = '*';
    $menuTitle = $menu->quickOverview($where);
    $pageTitle = new beheerPages();
    $pageTitle = $pageTitle->detailOverview($menuData['FK_pages_id']);
    $pageTitle = $pageTitle['page_title'];
    $parentItem = $menu->detailOverview($menuData['parent_id']);
    $parentItem = $parentItem['name'];

    ?>
    <div class="container-fluid">
        <div class="alert alert-success alert-dismissable <?php if(!isset($error['code'])){echo 'hidden';}?>">
            <strong><?php echo $error['message']; ?></strong>
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
        <form action="" method="post" autocomplete="off">
            <div class="page_update">
                <div class="row">
                    <div class="col-md-4">
                        <h1>Name</h1>
                        <input type="text" name="name" value="<?php echo $menuData['name'] ?>">
                    </div>
                    <div class="col-md-4">
                        <h1>Linked page</h1>
                        <select name="FK_pages_id">
                            <?php
                                echo '<option>'.$pageTitle.'</option>';
                                $pages = new beheerPages();
                                $pages = $pages->quickOverview();
                                foreach($pages AS $item){
                                    echo '<option value="'.$item['id'].'">'.$item['page_title'].'</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <h1>parent item</h1>
                        <select name="">
                            <option></option>
                        </select>
                    </div>
                </div>
            </div>
    </div>

    <div class="submit">
        <div>
            <input type="hidden" name="flag" value="send">
            <input type="submit" value="save">
        </div>
    </div>
        </form>

<?php } ?>
