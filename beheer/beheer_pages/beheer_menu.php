<!-- RIJ 1 -->
<div class="row">
  <div class="col-xs-12 col-sm-4 col-lg-6">
    <a href="/beheer/page">
      <div class="beheer-option-F beheer-option">
        <h1>Pages</h1>
        <i class="fa fa-5x fa-files-o" aria-hidden="true"></i>
      </div>
    </a>
  </div>
  <div class="col-xs-12 col-sm-4 col-lg-6">
    <a href="/beheer/menu">
      <div class="beheer-option-B beheer-option">
        <h1>Menu</h1>
        <i class="fa fa-5x fa-compass" aria-hidden="true"></i>
      </div>
    </a>
  </div>
  <div class="col-xs-12 col-sm-4 hidden-lg">
    <a href="/beheer/article">
      <div class="beheer-option-C beheer-option">
        <h1>Articles</h1>
        <i class="fa fa-5x fa-newspaper-o" aria-hidden="true"></i>
      </div>
    </a>
  </div>
  <div class="col-xs-12 col-sm-4 hidden-lg">
    <a href="/beheer/user">
      <div class="beheer-option-E beheer-option">
        <h1>Users</h1>
        <i class="fa fa-5x fa-users" aria-hidden="true"></i>
      </div>
    </a>
  </div>
  <div class="col-xs-12 col-sm-4 hidden-lg">
    <a href="/beheer/module">
      <div class="beheer-option-A beheer-option">
        <h1>Modules</h1>
        <i class="fa fa-5x fa-cogs" aria-hidden="true"></i>
      </div>
    </a>
  </div>
  <div class="col-xs-12 col-sm-4 hidden-lg">
    <a href="/beheer/inbox">
      <div class="beheer-option-D beheer-option">
        <h1>Inbox</h1>
        <i class="fa fa-5x fa-inbox" aria-hidden="true"></i>
      </div>
    </a>
  </div>
</div>
