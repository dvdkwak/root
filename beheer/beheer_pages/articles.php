<?php
$urlCheck = explode("/", $url);
$params = $url;
$params = substr($params, 1);
$params = explode('/', $params);

$news = new beheerNews();

if( $url == "/article" || $urlCheck['2'] == "remove") {
    //THIS SECTION SHOWS A TABLE WITH AN OVERVIEW OF ALL PAGES IN THE SYSTEM
    $tableRow = $news->quickOverview();

    if(in_array("remove", $params)){
        $error = array("code"=>"2", "message"=>"Are you sure to <strong>remove</strong> the article: <strong>".$params['3']."</strong>?");
        if(isset($_POST['action']) && $_POST['action'] == "remove"){
            $error = $news->removeArticle($params['2'], $params['3']);
            header("location:/beheer/article");
        }
    }
    ?>
    <div class="container-fluid">
        <h1>Overview of all articles on this site.</h1>
        <a href="article/add">
            <div class="btn alert-success pull-right">
                <i class="fa fa-plus" aria-hidden="true"></i>  Add
            </div>
        </a>
        <hr>
        <div class="alert alert-success alert-dismissable <?php if(!isset($error['code'])){echo 'hidden';}?>">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $error['message']; ?>
            <form class="pull-right" method="post">
                <input type="hidden" name="action" value="remove">
                <input type="submit" value="Yes">
            </form>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th><p>title</p></th>
                <th><p>summary</p></th>
                <th><p>thumb nail</p></th>
                <th class="text-center"><p>delete</p></th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($tableRow as $row) {
                echo '<tr class="table_hover">';
                foreach (array_slice($row, 1) as $item) {
                    echo '<td><a href="/beheer/article/' . $row['id'] . '">';
                    echo $item;
                    echo '</a></td>';
                }
                echo '<td>
                    <a href="/beheer/article/remove/'.$row['id'].'/'.$row['title'].'" class="table_thrash">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </a>
                      </td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
    <?php
}else{
    //__________________________________________ SECTION FOR EXISTING PAGES (UPDATES) _________________________________

    if(isset($_POST['flag']) && $_POST['flag'] == "send"){
        if($params[1] != "add") {
            $error = $news->saveChanges($params['1'], $_POST['title'], $_POST['summary'], $_POST['content'],
                $_POST['thumb_nail'], $_POST['author']);
        }else{
            $error = $news->addArticle($_POST['title'], $_POST['summary'], $_POST['content'],
                $_POST['thumb_nail'], $_POST['author']);
        }
    }

    $articleData = $news->detailOverview($params['1']);
    ?>
    <div class="container-fluid">
        <div class="alert alert-success alert-dismissable <?php if(!isset($error['code'])){echo 'hidden';}?>">
            <strong><?php echo $error['message']; ?></strong>
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
        <form action="" method="post" autocomplete="off">
        <div class="page_update">
            <div class="row">
                <div class="col-md-4">
                    <h1>Title</h1>
                    <input type="text" name="title" value="<?php echo $articleData['title'] ?>">
                </div>
                <div class="col-md-4">
                    <h1>Thumb Nail</h1>
                    <input type="text" name="thumb_nail" value="<?php echo $articleData['thumb_nail'] ?>">
                </div>
                <div class="col-md-4">
                    <h1>Author</h1>
                    <input type="text" name="author" value="<?php echo $articleData['author'] ?>">
                </div>
            </div>
            <div class="row">
                <h1>Summary</h1>
                <textarea name="summary"><?php echo $articleData['summary'] ?></textarea>
            </div>
        </div>
        <div class="page_update">
            <div class="row">
                <h1>Content</h1>
                <textarea name="content"><?php echo $articleData['content'] ?></textarea>
            </div>
        </div>
    </div>

    <div class="submit">
        <div>
            <input type="hidden" name="flag" value="send">
            <input type="submit" value="save">
        </div>
    </div>
    </form>

<?php } ?>
