$(document).ready(function(){


  $(window).resize(function(){

    //Shrinking the title on mobile devices
    if( $(window).width() <= 530 ){
      $(".beheer-header h1").text( "YouCode." );
    }
    if( $(window).width() >= 531 ){
      $(".beheer-header h1").text( "Welcome at YouCode." );
    }

    //setting the footer hight and width on desktop
    if( $(window).width() > 1200 ){
      //beheer-support-header op gelijke hoogte van header zetten
      var wH = $(window).height();
      var hH = $(".beheer-header-rood").height();
      var cH = $(".beheer-content").outerHeight();
      var tH = wH - ( hH + cH );

      if( tH < wH ){
        $(".beheer-supp-header").css("position", "fixed");
        $(".beheer-supp-header").css("width", "100%");
        $(".beheer-supp-header").css("bottom", "0");
      }


      //footer de juist hoogte hebben
      var wH = $(window).height();
      var hH = $(".beheer-header-rood").height();
      var oH = $(".beheer-option").outerHeight();
      var oH = 3*oH;
      var tH = wH-(hH+oH);

      if( tH < 50 ){
        $(".beheer-footer").height( 50 );
        $(".beheer-supp-header").height( 50 );
      }else{
        $(".beheer-footer").height( tH );
        $(".beheer-supp-header").height( tH );
      }

      var oW = $(".beheer-option").outerWidth();
      var oW = 2*oW;

      $(".beheer-footer").width( oW );
    }

    //setting footerhight for tablets
    if( $(window).width() > 768 && $(window).width() < 1200 ){
      //beheer-support-header dingest weer goed zetten
      $(".beheer-supp-header").css("position", "relative");
      $(".beheer-supp-header").css("width", "auto");
      $(".beheer-supp-header").css("bottom", "");

      //footer de juist hoogte hebben
      var wH = $(window).height();
      var hH = $(".beheer-header-rood").height();
      var oH = $(".beheer-option").outerHeight();
      var oH = 2*oH;
      var shH = $(".beheer-supp-header").outerHeight();
      var tH = wH-(hH+oH+shH);

      if( tH < 50 ){
        $(".beheer-footer").height( 50 );
        $(".beheer-supp-header").height( 50 );
      }else{
        $(".beheer-footer").height( tH );
        $(".beheer-supp-header").height( tH );
      }

      $(".beheer-footer").width( "auto" );
    }

    //setting footerhight for mobiel devices
    if( $(window).width() < 768 ){
      $(".beheer-footer").height( 50 );
      $(".beheer-supp-header").height( 50 );

      //beheer-support-header dingest weer goed zetten
      $(".beheer-supp-header").css("position", "relative");
      $(".beheer-supp-header").css("width", "auto");
      $(".beheer-supp-header").css("bottom", "");
    }

  }); //_________________ EINDE RESIZE FUNCTIE ____________________

  //setting the footer hight and width on desktop
  if( $(window).width() > 1200 ){
    //beheer-support-header op gelijke hoogte van header zetten
    var wH = $(window).height();
    var hH = $(".beheer-header-rood").height();
    var cH = $(".beheer-content").outerHeight();
    var tH = wH - ( hH + cH );

    if( tH < wH ){
      $(".beheer-supp-header").css("position", "fixed");
      $(".beheer-supp-header").css("width", "100%");
      $(".beheer-supp-header").css("bottom", "0");
    }

    //footer de juist hoogte hebben
    var wH = $(window).height();
    var hH = $(".beheer-header-rood").height();
    var oH = $(".beheer-option").outerHeight();
    var oH = 3*oH;
    var tH = wH-(hH+oH);

    if( tH < 50 ){
      $(".beheer-footer").height( 50 );
      $(".beheer-supp-header").height( 50 );
    }else{
      $(".beheer-footer").height( tH );
      $(".beheer-supp-header").height( tH );
    }

    var oW = $(".beheer-option").outerWidth();
    var oW = 2*oW;

    $(".beheer-footer").width( oW );
  }

  //Shrinking title on mobile devices
  if( $(window).width() <= 530 ){
    $(".beheer-header h1").text( "YouCode." );
  }
  if( $(window).width() >= 531 ){
    $(".beheer-header h1").text( "Welcome at YouCode." );
  }

  //setting footerhight for tablets
  if( $(window).width() > 768 && $(window).width() < 1200 ){
    //footer de juist hoogte hebben
    var wH = $(window).height();
    var hH = $(".beheer-header-rood").height();
    var oH = $(".beheer-option").outerHeight();
    var oH = 2*oH;
    var shH = $(".beheer-supp-header").outerHeight();
    var tH = wH-(hH+oH+shH);

    if( tH < 50 ){
      $(".beheer-footer").height( 50 );
      $(".beheer-supp-header").height( 50 );
    }else{
      $(".beheer-footer").height( tH );
      $(".beheer-supp-header").height( tH );
    }
  }

  //setting footerhight for mobiel devices
  if( $(window).width() < 768 ){
    $(".beheer-footer").height( 50 );
    $(".beheer-supp-header").height( 50 );
  }

});
