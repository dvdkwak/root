<?php

class agenda extends db
{
    public function showMonth( $month )
    {
        switch ( $month ):
            case 1:
                $this->month = "January";
                $this->Tdays = 31;
                break;
            case 2:
                $this->month = "February";
                $this->Tdays = 28;
                break;
            case 3:
                $this->month = "March";
                $this->Tdays = 31;
                break;
            case 4:
                $this->month = "April";
                $this->Tdays = 30;
                break;
            case 5:
                $this->month = "May";
                $this->Tdays = 31;
                break;
            case 6:
                $this->month = "June";
                $this->Tdays = 30;
                break;
            case 7:
                $this->month = "July";
                $this->Tdays = 31;
                break;
            case 8:
                $this->month = "August";
                $this->Tdays = 31;
                break;
            case 9:
                $this->month = "September";
                $this->Tdays = 30;
                break;
            case 10:
                $this->month = "October";
                $this->Tdays = 31;
                break;
            case 11:
                $this->month = "November";
                $this->Tdays = 30;
                break;
            case 12:
                $this->month = "December";
                $this->Tdays = 31;
                break;
            endswitch;

        $data = array( "Month"=>$this->month, "Tdays"=>$this->Tdays );
        return $data;
    }

    public function showEvents( $month )
    {
        $query = 'SELECT * FROM tbl_agenda';
        $mysqli = $this->connect();
        $result = $mysqli->query( $query );
        $result = $result->fetch_assoc();
        $result = $result["date"];
        var_dump($result);
    }
}