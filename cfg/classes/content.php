<?php

  class content extends db{

    //Method for gaining the page's content
    public function getContent( $url ){

      //connection gained from db class
      $mysqli = $this->connect();

      $mysqli->real_escape_string($url);
	
      //filling the content class according to the given url
      $query = 'SELECT * FROM tbl_pages WHERE url = "'. $url .'"';
      $result = $mysqli->query( $query );

      $data = $result->fetch_assoc();

      if( empty( $data ) ){
        //Loading the 404 page
        $query = 'SELECT * FROM tbl_pages WHERE url = "/404"';
        $result = $mysqli->query( $query );

        $data = $result->fetch_assoc();


      }
      //loading the page accoding to given url
      $this->meta_title = $data['meta_title'];
      $this->content = $data['content'];
      $this->module = $data['FK_module_id'];
      $this->lock = $data['lock'];
      $this->lock_location = $data['lock_location'];
    }


    public function getModule(){

      //connection gained from db class
	  $mysqli = $this->connect();

      if( empty( $this->module ) ){
        $this->errorMessage .= "Geen module gevonden";
      }else{
        $query = 'SELECT * FROM tbl_modules WHERE id = "'. $this->module .'"';
        $result = $mysqli->query( $query );

        $data = $result->fetch_assoc();

        //this will return the module path
        $return = $data['path'];
        return $return;
      }

    }

    public function getMenu($where){
      $mysqli = $this->connect();
      $query = 'SELECT * FROM tbl_menu WHERE '.$where;
      $result = $mysqli->query($query);
      while($data = $result->fetch_assoc()){
          $return[] = $data;
      }
      if(empty($return)){
          $return = "";
      }
      return $return;
    }

  }

?>
