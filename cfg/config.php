<?php

session_start();

//set the basic variables
$servername   = "localhost";
$username     = "root";
$password     = "usbw";
$dbname       = "cms";

$root = $_SERVER["DOCUMENT_ROOT"];

//Url variabele voor de Content class
if( empty( $_GET['url'] ) || $_GET['url'] == "/home" ){
  $url = "/home";
}else{
  $url = "/" . $_GET['url'];
}

//Includes van de Functies en/of Objecten _________________ <=- PUT INCLUDES TO YOUR MODULES HERE
include_once ( "classes/db.php" );
include_once ( "classes/content.php" );
include_once ( "classes/user.php" );
include_once ( "classes/agenda.php" );
include_once ( "classes/news.php" );
include_once("/../cfg/functions/function_menu.php");

//Default time zones
date_default_timezone_set('UTC');

?>
