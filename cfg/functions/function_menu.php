<?php

function buildMenu()
{
    $content = new content();
    echo '<ul>';
    $where = 'parent_id=0';
    $items = $content->getMenu($where);
    foreach($items AS $li){
        menuBuilder($content, $li);
    }
    echo '</ul>';
}

function menuBuilder($content, $items){
    echo '<li>';
    echo $items['name'];
    $where = 'parent_id=' . $items['id'];
    $result = $content->getMenu($where);
    if (!empty($result)) {
        echo '<ul>';
        foreach($result AS $items){
            menuBuilder($content, $items);
        }
        echo '</ul>';
    }
    echo '</li>';
}