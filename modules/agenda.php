<div class="container">

<?php
$date = date("d/m/Y");
$date = explode("/", $date);
$agenda = new agenda;
$data = $agenda->showMonth($date['1']);

echo 'Demo of the $agenda->showMonth, which returns an object containing month name and total days in the Month<br><br>';
var_dump($data);

echo "<br><br><hr>";

?>

    <h1><?php echo $data["Month"]; ?></h1>
    <br>

    <?php
    echo '<table class="table">';
    echo '<thead>';
        echo '<tr>';
            echo '<th>Maandag</th>';
            echo '<th>Dinsdag</th>';
            echo '<th>Woensdag</th>';
            echo '<th>Donderdag</th>';
            echo '<th>Vrijdag</th>';
            echo '<th>Zaterdag</th>';
            echo '<th>Zondag</th>';
        echo '</tr>';
    echo '</thead>';
    echo '<tbody>';
    $tellerA = 1;
    $tellerB = 1;
    while( $tellerA <= $data["Tdays"])
    {
        echo '<tr>';
        while( $tellerB <= 7)
        {
            echo '<td>test</td>';
            $tellerA += 1;
            $tellerB += 1;
        }
        echo '</tr>';
        $tellerB = 1;
    }
    echo '<tbody>';
    echo '</table>';

    echo '<br><br>';

    $item = $agenda->showEvents( 9 );
    ?>

</div>
