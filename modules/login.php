<?php
	include_once( "cfg/config.php" );

	$user = new user();

	if( isset( $_POST['login'] ) && $_POST['login'] == "go" ){
		$user->login( $_POST['username'], $_POST['password'], $_SESSION['oldLocation'] );
	}
?>
<div class="itm_title">
	<h1>Website naam.</h1>
</div>
<div class="itm_LoginBox">
	<h1>Login</h1>
	<form method="POST">
		<input class="itm_input" type="text" placeholder="Naam" name="username" autocomplete="off">
		<input class="itm_input" type="password" placeholder="wachtwoord" name="password" autocomplete="off">
		<input type="hidden" name="login" value="go">
		<a href="#"><p>Wachtwoord vergeten?</p></a>
		<input class="itm_submit" type="submit" value="Login">
	</form>
</div>
<div class="itm_disclaimer">
	<h1>- Made by David Kwakernaak -</h1>
</div>
