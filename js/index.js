$(document).ready(function(){

	//Getting height of half the screen
	wH = $(window).height();

	//calculating login box height
	bH = $(".itm_LoginBox").outerHeight();

	//calulating and setting needed hight
	height = ( wH - bH ) / 2;
	$(".itm_LoginBox").css("margin-top", height );


	//Same code but then for resize event

	$(window).resize(function(){

		//Getting height of half the screen
		wH = $(window).height();

		//calculating login box height
		bH = $(".itm_LoginBox").outerHeight();

		//calulating and setting needed hight
		height = ( wH - bH ) / 2;
		$(".itm_LoginBox").css("margin-top", height );

	});

});
