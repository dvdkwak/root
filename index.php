<?php
  //config includen
include_once( "cfg/config.php" );

$content = new content();
$content->getContent($url);

if($content->lock)
{
$user = new user;
$user->lock($content->lock_location, $url);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php //echo $root; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php //echo $root; ?>/fontawesome/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php //echo $root; ?>/css/index.css" rel="stylesheet">
    <?php
    if( isset( $url ) && $url == "/login" ){
        echo '<link href="/css/inlog.css" rel="stylesheet">';
    }
    ?>
    <!-- title van de site -->
    <title><?php echo $content->meta_title; ?></title>
</head>
<body>

<?php

buildMenu();

$modulePath = $content->getModule();

if( !empty( $content->content ) ){
    echo $content->content;
}
if( !empty( $modulePath ) ){
    include_once( "modules" . $modulePath );
}

?>

</body>
<script src="<?php //echo $root; ?>jquery/jquery.min.js"></script>
<script src="<?php //echo $root; ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php //echo $root; ?>js/index.js"></script>
</html>
